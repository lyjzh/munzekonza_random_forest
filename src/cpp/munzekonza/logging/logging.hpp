//
// File:          munzekonza/logging/logging.hpp
// Author:        Marko Ristin
// Creation date: Feb 07 2013
//

#ifndef MUNZEKONZA_LOGGING_LOGGING_HPP_
#define MUNZEKONZA_LOGGING_LOGGING_HPP_

#include <boost/format.hpp>
#include <boost/preprocessor/cat.hpp>

#include <iostream>
#include <sstream>
#include <string>
#include <ctime>
#include <iomanip>
#include <cstring>
#include <stdexcept>

namespace munzekonza {
namespace logging {

const std::string cc_console_color_default = "\e[0m";
const std::string cc_fore_black            = "\e[30m";
const std::string cc_fore_blue             = "\e[34m";
const std::string cc_fore_red              = "\e[31m";
const std::string cc_fore_magenta          = "\e[35m";
const std::string cc_fore_green            = "\e[32m";
const std::string cc_fore_cyan             = "\e[36m";
const std::string cc_fore_yellow           = "\e[33m";
const std::string cc_fore_white            = "\e[37m";
const std::string cc_fore_console          = "\e[39m";

const std::string cc_fore_lightblack       = "\e[90m";
const std::string cc_fore_lightblue        = "\e[94m";
const std::string cc_fore_lightred         = "\e[91m";
const std::string cc_fore_lightmagenta     = "\e[95m";
const std::string cc_fore_lightgreen       = "\e[92m";
const std::string cc_fore_lightcyan        = "\e[96m";
const std::string cc_fore_lightyellow      = "\e[93m";
const std::string cc_fore_lightwhite       = "\e[97m";

const std::string cc_back_black            = "\e[40m";
const std::string cc_back_blue             = "\e[44m";
const std::string cc_back_red              = "\e[41m";
const std::string cc_back_magenta          = "\e[45m";
const std::string cc_back_green            = "\e[42m";
const std::string cc_back_cyan             = "\e[46m";
const std::string cc_back_yellow           = "\e[43m";
const std::string cc_back_white            = "\e[47m";
const std::string cc_back_console          = "\e[49m";

const std::string cc_back_lightblack       = "\e[100m";
const std::string cc_back_lightblue        = "\e[104m";
const std::string cc_back_lightred         = "\e[101m";
const std::string cc_back_lightmagenta     = "\e[105m";
const std::string cc_back_lightgreen       = "\e[102m";
const std::string cc_back_lightcyan        = "\e[106m";
const std::string cc_back_lightyellow      = "\e[103m";
const std::string cc_back_lightwhite       = "\e[107m";
} // namespace logging
} // namespace munzekonza

#define MUNZEKONZA_TIMENOW_ID(x)  BOOST_PP_CAT(x, __LINE__)

#define MUNZEKONZA_SHORT_FORM_OF_FILE \
  (std::strrchr(__FILE__,'/') \
   ? std::strrchr(__FILE__,'/')+1 \
   : __FILE__ \
  )

#define MUNZEKONZA_COMPUTE_TIME_NOW() \
time_t MUNZEKONZA_TIMENOW_ID(_raw_time);\
struct tm* MUNZEKONZA_TIMENOW_ID(_timeinfo);\
char MUNZEKONZA_TIMENOW_ID(_buffer)[512];\
time(&MUNZEKONZA_TIMENOW_ID(_raw_time));\
MUNZEKONZA_TIMENOW_ID(_timeinfo) = localtime(&MUNZEKONZA_TIMENOW_ID(_raw_time));\
strftime( MUNZEKONZA_TIMENOW_ID(_buffer), 200, "%y-%m-%d %H:%M:%S", MUNZEKONZA_TIMENOW_ID(_timeinfo) );\


#define DEBUG1(message) \
do{\
MUNZEKONZA_COMPUTE_TIME_NOW()\
std::cout << munzekonza::logging::cc_fore_blue << MUNZEKONZA_SHORT_FORM_OF_FILE << ": " << \
          std::setw(5) << __LINE__  << ": " << \
          MUNZEKONZA_TIMENOW_ID(_buffer) << ": " << munzekonza::logging::cc_console_color_default << \
          (message) << std::endl;\
} while(0)

#define DEBUG2(message, item0) \
do{\
MUNZEKONZA_COMPUTE_TIME_NOW()\
std::cout << munzekonza::logging::cc_fore_blue << MUNZEKONZA_SHORT_FORM_OF_FILE << ": " << \
          std::setw(5) << __LINE__  << ": " << \
          MUNZEKONZA_TIMENOW_ID(_buffer) << ": " << munzekonza::logging::cc_console_color_default << \
          boost::format(message) % (item0) << std::endl;\
} while(0)

#define DEBUG3(message, item0, item1) \
do{\
MUNZEKONZA_COMPUTE_TIME_NOW()\
std::cout << munzekonza::logging::cc_fore_blue << MUNZEKONZA_SHORT_FORM_OF_FILE << ": " << \
          std::setw(5) << __LINE__  << ": " << \
          MUNZEKONZA_TIMENOW_ID(_buffer) << ": " << munzekonza::logging::cc_console_color_default << \
          boost::format(message) % \
          (item0) % (item1) << std::endl;\
} while(0)


#define DEBUG4(message, item0, item1, item2) \
do{\
MUNZEKONZA_COMPUTE_TIME_NOW()\
std::cout << munzekonza::logging::cc_fore_blue << MUNZEKONZA_SHORT_FORM_OF_FILE << ": " << \
          std::setw(5) << __LINE__  << ": " << \
          MUNZEKONZA_TIMENOW_ID(_buffer) << ": " << munzekonza::logging::cc_console_color_default << \
          boost::format(message) % \
          (item0) % (item1) % (item2) << std::endl;\
} while(0)


#define DEBUG5(message, item0, item1, item2, item3) \
do{\
MUNZEKONZA_COMPUTE_TIME_NOW()\
std::cout << munzekonza::logging::cc_fore_blue << MUNZEKONZA_SHORT_FORM_OF_FILE << ": " << \
          std::setw(5) << __LINE__  << ": " << \
          MUNZEKONZA_TIMENOW_ID(_buffer) << ": " << munzekonza::logging::cc_console_color_default << \
          boost::format(message) % \
          (item0) % (item1) % (item2) % \
          (item3) << std::endl;\
} while(0)


#define DEBUG6(message, item0, item1, item2, item3, item4) \
do{\
MUNZEKONZA_COMPUTE_TIME_NOW()\
std::cout << munzekonza::logging::cc_fore_blue << MUNZEKONZA_SHORT_FORM_OF_FILE << ": " << \
          std::setw(5) << __LINE__  << ": " << \
          MUNZEKONZA_TIMENOW_ID(_buffer) << ": " << munzekonza::logging::cc_console_color_default << \
          boost::format(message) % \
          (item0) % (item1) % (item2) % \
          (item3) % (item4) << std::endl;\
} while(0)


#define DEBUG7(message, item0, item1, item2, item3, item4, item5) \
do{\
MUNZEKONZA_COMPUTE_TIME_NOW()\
std::cout << munzekonza::logging::cc_fore_blue << MUNZEKONZA_SHORT_FORM_OF_FILE << ": " << \
          std::setw(5) << __LINE__  << ": " << \
          MUNZEKONZA_TIMENOW_ID(_buffer) << ": " << munzekonza::logging::cc_console_color_default << \
          boost::format(message) % \
          (item0) % (item1) % (item2) % \
          (item3) % (item4) % (item5) << std::endl;\
} while(0)

// variadic DEBUG  macro
#define GET_8TH_ARG(el0, el1, el2, el3, el4, el5, el6, FUNC, ...) FUNC
#define DEBUG_SELECTOR(...) \
  GET_8TH_ARG(__VA_ARGS__, DEBUG7, DEBUG6, DEBUG5, DEBUG4, DEBUG3, DEBUG2, DEBUG1, )

#define DEBUG(...) DEBUG_SELECTOR(__VA_ARGS__)(__VA_ARGS__)

#define SAY1(message) \
do{\
MUNZEKONZA_COMPUTE_TIME_NOW()\
std::cout << munzekonza::logging::cc_fore_blue << MUNZEKONZA_SHORT_FORM_OF_FILE << ": " << \
          std::setw(5) << __LINE__  << ": " << \
          MUNZEKONZA_TIMENOW_ID(_buffer) << ": " << munzekonza::logging::cc_console_color_default << \
          (message) << std::endl;\
} while(0)


#define SAY2(message, item0) \
do{\
MUNZEKONZA_COMPUTE_TIME_NOW()\
std::cout << munzekonza::logging::cc_fore_blue << MUNZEKONZA_SHORT_FORM_OF_FILE << ": " << \
          std::setw(5) << __LINE__  << ": " << \
          MUNZEKONZA_TIMENOW_ID(_buffer) << ": " << munzekonza::logging::cc_console_color_default << \
          boost::format(message) % (item0) << std::endl;\
} while(0)


#define SAY3(message, item0, item1) \
do{\
MUNZEKONZA_COMPUTE_TIME_NOW()\
std::cout << munzekonza::logging::cc_fore_blue << MUNZEKONZA_SHORT_FORM_OF_FILE << ": " << \
          std::setw(5) << __LINE__  << ": " << \
          MUNZEKONZA_TIMENOW_ID(_buffer) << ": " << munzekonza::logging::cc_console_color_default << \
          boost::format(message) % \
          (item0) % (item1) << std::endl;\
} while(0)


#define SAY4(message, item0, item1, item2) \
do{\
MUNZEKONZA_COMPUTE_TIME_NOW()\
std::cout << munzekonza::logging::cc_fore_blue << MUNZEKONZA_SHORT_FORM_OF_FILE << ": " << \
          std::setw(5) << __LINE__  << ": " << \
          MUNZEKONZA_TIMENOW_ID(_buffer) << ": " << munzekonza::logging::cc_console_color_default << \
          boost::format(message) % \
          (item0) % (item1) % (item2) << std::endl;\
} while(0)


#define SAY5(message, item0, item1, item2, item3) \
do{\
MUNZEKONZA_COMPUTE_TIME_NOW()\
std::cout << munzekonza::logging::cc_fore_blue << MUNZEKONZA_SHORT_FORM_OF_FILE << ": " << \
          std::setw(5) << __LINE__  << ": " << \
          MUNZEKONZA_TIMENOW_ID(_buffer) << ": " << munzekonza::logging::cc_console_color_default << \
          boost::format(message) % \
          (item0) % (item1) % (item2) % \
          (item3) << std::endl;\
} while(0)


#define SAY6(message, item0, item1, item2, item3, item4) \
do{\
MUNZEKONZA_COMPUTE_TIME_NOW()\
std::cout << munzekonza::logging::cc_fore_blue << MUNZEKONZA_SHORT_FORM_OF_FILE << ": " << \
          std::setw(5) << __LINE__  << ": " << \
          MUNZEKONZA_TIMENOW_ID(_buffer) << ": " << munzekonza::logging::cc_console_color_default << \
          boost::format(message) % \
          (item0) % (item1) % (item2) % \
          (item3) % (item4) << std::endl;\
} while(0)


#define SAY7(message, item0, item1, item2, item3, item4, item5) \
do{\
MUNZEKONZA_COMPUTE_TIME_NOW()\
std::cout << munzekonza::logging::cc_fore_blue << MUNZEKONZA_SHORT_FORM_OF_FILE << ": " << \
          std::setw(5) << __LINE__  << ": " << \
          MUNZEKONZA_TIMENOW_ID(_buffer) << ": " << munzekonza::logging::cc_console_color_default << \
          boost::format(message) % \
          (item0) % (item1) % (item2) % \
          (item3) % (item4) % (item5) << std::endl;\
} while(0)


// variadic SAY  macro
#define GET_8TH_ARG(el0, el1, el2, el3, el4, el5, el6, FUNC, ...) FUNC
#define SAY_SELECTOR(...) \
  GET_8TH_ARG(__VA_ARGS__, SAY7, SAY6, SAY5, SAY4, SAY3, SAY2, SAY1, )

#define SAY(...) SAY_SELECTOR(__VA_ARGS__)(__VA_ARGS__)


#define THROW1(message) \
do{\
std::stringstream ss;\
MUNZEKONZA_COMPUTE_TIME_NOW()\
ss << munzekonza::logging::cc_fore_blue << MUNZEKONZA_SHORT_FORM_OF_FILE << ": " << \
          std::setw(5) << __LINE__  << ": " << \
          MUNZEKONZA_TIMENOW_ID(_buffer) << ": " << munzekonza::logging::cc_console_color_default << \
          (message) << std::endl;\
throw std::runtime_error(ss.str());\
} while(0)


#define THROW2(message, item0) \
do{\
std::stringstream ss;\
MUNZEKONZA_COMPUTE_TIME_NOW()\
ss << munzekonza::logging::cc_fore_blue << MUNZEKONZA_SHORT_FORM_OF_FILE << ": " << \
          std::setw(5) << __LINE__  << ": " << \
          MUNZEKONZA_TIMENOW_ID(_buffer) << ": " << munzekonza::logging::cc_console_color_default << \
          boost::format(message) % (item0) << std::endl;\
throw std::runtime_error(ss.str());\
} while(0)


#define THROW3(message, item0, item1) \
do{\
std::stringstream ss;\
MUNZEKONZA_COMPUTE_TIME_NOW()\
ss << munzekonza::logging::cc_fore_blue << MUNZEKONZA_SHORT_FORM_OF_FILE << ": " << \
          std::setw(5) << __LINE__  << ": " << \
          MUNZEKONZA_TIMENOW_ID(_buffer) << ": " << munzekonza::logging::cc_console_color_default << \
          boost::format(message) % \
          (item0) % (item1) << std::endl;\
throw std::runtime_error(ss.str());\
} while(0)


#define THROW4(message, item0, item1, item2) \
do{\
std::stringstream ss;\
MUNZEKONZA_COMPUTE_TIME_NOW()\
ss << munzekonza::logging::cc_fore_blue << MUNZEKONZA_SHORT_FORM_OF_FILE << ": " << \
          std::setw(5) << __LINE__  << ": " << \
          MUNZEKONZA_TIMENOW_ID(_buffer) << ": " << munzekonza::logging::cc_console_color_default << \
          boost::format(message) % \
          (item0) % (item1) % (item2) << std::endl;\
throw std::runtime_error(ss.str());\
} while(0)


#define THROW5(message, item0, item1, item2, item3) \
do{\
std::stringstream ss;\
MUNZEKONZA_COMPUTE_TIME_NOW()\
ss << munzekonza::logging::cc_fore_blue << MUNZEKONZA_SHORT_FORM_OF_FILE << ": " << \
          std::setw(5) << __LINE__  << ": " << \
          MUNZEKONZA_TIMENOW_ID(_buffer) << ": " << munzekonza::logging::cc_console_color_default << \
          boost::format(message) % \
          (item0) % (item1) % (item2) % \
          (item3) << std::endl;\
throw std::runtime_error(ss.str());\
} while(0)


#define THROW6(message, item0, item1, item2, item3, item4) \
do{\
std::stringstream ss;\
MUNZEKONZA_COMPUTE_TIME_NOW()\
ss << munzekonza::logging::cc_fore_blue << MUNZEKONZA_SHORT_FORM_OF_FILE << ": " << \
          std::setw(5) << __LINE__  << ": " << \
          MUNZEKONZA_TIMENOW_ID(_buffer) << ": " << munzekonza::logging::cc_console_color_default << \
          boost::format(message) % \
          (item0) % (item1) % (item2) % \
          (item3) % (item4) << std::endl;\
throw std::runtime_error(ss.str());\
} while(0)


#define THROW7(message, item0, item1, item2, item3, item4, item5) \
do{\
std::stringstream ss;\
MUNZEKONZA_COMPUTE_TIME_NOW()\
ss << munzekonza::logging::cc_fore_blue << MUNZEKONZA_SHORT_FORM_OF_FILE << ": " << \
          std::setw(5) << __LINE__  << ": " << \
          MUNZEKONZA_TIMENOW_ID(_buffer) << ": " << munzekonza::logging::cc_console_color_default << \
          boost::format(message) % \
          (item0) % (item1) % (item2) % \
          (item3) % (item4) % (item5) << std::endl;\
throw std::runtime_error(ss.str());\
} while(0)


// variadic THROW  macro
#define GET_8TH_ARG(el0, el1, el2, el3, el4, el5, el6, FUNC, ...) FUNC
#define THROW_SELECTOR(...) \
  GET_8TH_ARG(__VA_ARGS__, THROW7, THROW6, THROW5, THROW4, THROW3, THROW2, THROW1, )

#define THROW(...) THROW_SELECTOR(__VA_ARGS__)(__VA_ARGS__)

#endif // MUNZEKONZA_LOGGING_LOGGING_HPP_
