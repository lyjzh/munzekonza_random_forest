//
// File:          munzekonza/random_forest/entropier.cpp
// Author:        Marko Ristin
// Creation date: Sep 23 2014
//

#include "munzekonza/random_forest/entropier.hpp"

#include "munzekonza/debugging/assert.hpp"

#include <cmath>

namespace munzekonza {
namespace random_forest {

void Entropier::set_nsamples( int a_nsamples ) {
  nsamples = a_nsamples;
}

void Entropier::compute_nsamples_at_assignment(
  const Eigen::MatrixXi& partition_class_histos ) {

  nsamples_at_assignment_0 = partition_class_histos.block( 0, class_range_begin, 1, nclasses ).sum();
  nsamples_at_assignment_1 = nsamples - nsamples_at_assignment_0;
}

double Entropier::compute_split_entropy( const Eigen::MatrixXi& partition_class_histos ) const {
  ASSERT_GE( nsamples_at_assignment_0, 0 );
  ASSERT_GE( nsamples_at_assignment_1, 0 );
  ASSERT_GE( nsamples, 1 );

  double entropy_0 = 0;
  if( nsamples_at_assignment_0 > 0 ) {

    for( int label_i = class_range_begin; label_i < class_range_end; ++label_i ) {
      const int nobservations = partition_class_histos( 0, label_i );
      if( nobservations > 0 ) {
        const double dw_nobservations = nobservations;
        entropy_0 += -dw_nobservations * std::log( dw_nobservations );
      }
    }
    entropy_0 /= double( nsamples_at_assignment_0 );
    entropy_0 += std::log( double( nsamples_at_assignment_0 ) );
  }

  double entropy_1 = 0;
  if( nsamples_at_assignment_1 > 0 ) {
    for( int label_i = class_range_begin; label_i < class_range_end; ++label_i ) {
      const int nobservations = partition_class_histos( 1, label_i );
      if( nobservations > 0 ) {
        const double dw_nobservations = nobservations;
        entropy_1 += -dw_nobservations * std::log( dw_nobservations );
      }
    }
    entropy_1 /= double( nsamples_at_assignment_1 );
    entropy_1 += std::log( double( nsamples_at_assignment_1 ) );
  }

  const double result(
    double( nsamples_at_assignment_0 ) / ( double( nsamples ) ) * entropy_0 +
    double( nsamples_at_assignment_1 ) / ( double( nsamples ) ) * entropy_1 );

  //ASSERT( !std::isnan( result ) );

  return result;
}

} // namespace random_forest
} // namespace munzekonza

