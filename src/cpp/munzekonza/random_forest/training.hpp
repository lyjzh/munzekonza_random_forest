//
// File:          munzekonza/random_forest/training.hpp
// Author:        Marko Ristin
// Creation date: Sep 16 2014
//

#ifndef MUNZEKONZA_RANDOM_FOREST_TRAINING_HPP_
#define MUNZEKONZA_RANDOM_FOREST_TRAINING_HPP_

#include <vector>

namespace munzekonza {

namespace random_forest {
class Split;
class Node;
class Training_result;

/// abstract class, used for procedures
class Training {
public:
  virtual void train(
    Node* node,
    const std::vector<int>& sample_ids,
    Split& split,
    Training_result& result ) = 0;

  virtual void on_level_start(
    bool silent,
    int depth ) {};

  virtual void on_level_finished(
    bool silent,
    int depth ) {};

  virtual ~Training() {}
};

} // namespace random_forest
} // namespace munzekonza 

#endif // MUNZEKONZA_RANDOM_FOREST_TRAINING_HPP_
