//
// File:          ./munzekonza/random_forest/leaf_stats_collector.hpp
// Author:        Marko Ristin
// Creation date: Sep 16 2014
//

#ifndef MUNZEKONZA_RANDOM_FOREST_LEAF_STATS_COLLECTOR_HPP_
#define MUNZEKONZA_RANDOM_FOREST_LEAF_STATS_COLLECTOR_HPP_

#include "leaf_stats.hpp"

#include <vector>
#include <map>

namespace munzekonza {

namespace random_forest {
class Node;
class Seeper_abstract;

class Leaf_stats_collector {
public:
  void collect(
    Node* root,
    const Seeper_abstract& seeper,
    const std::vector<int>& sample_ids,
    const std::vector<int>& labels,
    std::vector<Leaf_stat>& leaf_stats );

  void collect(
    Node* root,
    const Seeper_abstract& seeper,
    const std::vector<int>& sample_ids,
    std::map<Node*, std::vector<int> >& leaf_samples );

  void collect(
    Node* root,
    const Seeper_abstract& seeper,
    const std::vector<int>& sample_ids,
    std::map<int, std::vector<int> >& leaf_samples );
};

} // namespace random_forest
} // namespace munzekonza 

#endif // MUNZEKONZA_RANDOM_FOREST_LEAF_STATS_COLLECTOR_HPP_
