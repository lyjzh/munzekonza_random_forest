//
// File:          munzekonza/random_forest/entropier.hpp
// Author:        Marko Ristin
// Creation date: Sep 23 2014
//

#ifndef MUNZEKONZA_RANDOM_FOREST_ENTROPIER_HPP_
#define MUNZEKONZA_RANDOM_FOREST_ENTROPIER_HPP_


#include <Eigen/Core>
#include <Eigen/Dense>

namespace munzekonza {
namespace random_forest {

/// computes entropy in an optimized way.
/// It is assumed that you go over different assignemnts
/// (i.e. partition_class_histos change, but the set of
/// samples remain the same).
class Entropier {
public:
  const int class_range_begin;
  const int class_range_end;
  const int nclasses;

  /// set with set_nsamples()
  int nsamples;

  //@{
  /// Set with compute_nsamples_at_assignment().
  int nsamples_at_assignment_0;
  int nsamples_at_assignment_1;
  //@}

  /// \a a_class_range_begin and \a a_class_range_end refer
  /// to label indices in histograms.
  /// The entropy will be computed on this range.
  ///
  /// \a nsamples denotes the number of samples in the range
  /// before the split.
  Entropier(
    int a_class_range_begin,
    int a_class_range_end ):

    class_range_begin( a_class_range_begin ),
    class_range_end( a_class_range_end ),
    nclasses( class_range_end - class_range_begin ),
    nsamples( -1 ),
    nsamples_at_assignment_0( -1 ),
    nsamples_at_assignment_1( -1 )
  { }

  void set_nsamples( int a_nsamples );

  void compute_nsamples_at_assignment(
    const Eigen::MatrixXi& partition_class_histos );

  /// computes the entropy over the range of classes.
  ///
  /// \remark call compute_class_histo() and
  /// compute_nsamples_at_assignment() before.
  double compute_split_entropy(
    const Eigen::MatrixXi& partition_class_histos ) const;

  virtual ~Entropier() {}

private:
  Eigen::MatrixXi* centroid_class_histos_;
};

} // namespace random_forest
} // namespace munzekonza

#endif // MUNZEKONZA_RANDOM_FOREST_ENTROPIER_HPP_
