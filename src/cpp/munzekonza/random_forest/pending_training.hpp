//
// File:          munzekonza/random_forest/pending_training.hpp
// Author:        Marko Ristin
// Creation date: Sep 16 2014
//

#ifndef MUNZEKONZA_RANDOM_FOREST_PENDING_TRAINING_HPP_
#define MUNZEKONZA_RANDOM_FOREST_PENDING_TRAINING_HPP_

#include "munzekonza/random_forest/node.hpp"

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>

#include <cstddef>

namespace munzekonza {

namespace random_forest {

struct Pending_training {
  Pending_training() :
    node( NULL )
  {}

  Pending_training(
    Node* a_node,
    const std::vector<int>& a_sample_ids ):

    node( a_node ),
    sample_ids( a_sample_ids )
  {}

  Node* node;
  std::vector<int> sample_ids;

private:
  friend class boost::serialization::access;
  template<class Archive>
  void serialize( Archive & ar, const unsigned int version ) {
    ar & node;
    ar & sample_ids;
  }
};

} // namespace random_forest
} // namespace munzekonza 

#endif // MUNZEKONZA_RANDOM_FOREST_PENDING_TRAINING_HPP_
