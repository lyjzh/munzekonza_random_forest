//
// File:          munzekonza/random_forest/status.hpp
// Author:        Marko Ristin
// Creation date: Sep 16 2014
//

#ifndef MUNZEKONZA_RANDOM_FOREST_STATUS_HPP_
#define MUNZEKONZA_RANDOM_FOREST_STATUS_HPP_

namespace munzekonza {

namespace random_forest {
namespace tree_training {
struct Status {
  double training_time;
  int splitting_nodes_trained;

  Status() :
    training_time( -1.0 ),
    splitting_nodes_trained( 0 )
  {}
};
} // namespace tree_training 
} // namespace random_forest 
} // namespace munzekonza 

#endif // MUNZEKONZA_RANDOM_FOREST_STATUS_HPP_
