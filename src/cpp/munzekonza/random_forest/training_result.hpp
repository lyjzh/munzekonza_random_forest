//
// File:          munzekonza/random_forest/training_result.hpp
// Author:        Marko Ristin
// Creation date: Sep 16 2014
//

#ifndef MUNZEKONZA_RANDOM_FOREST_TRAINING_RESULT_HPP_
#define MUNZEKONZA_RANDOM_FOREST_TRAINING_RESULT_HPP_

#include "node.hpp"
#include "pending_training.hpp"

#include <cmath>

namespace munzekonza {

namespace random_forest {

struct Training_result {
  bool found_optimal_split;
  double entropy;
  double information_gain;
  std::vector<Pending_training> pending_trainings;

  Training_result() :
    found_optimal_split(false),
    entropy(NAN),
    information_gain(NAN)
  {}
};

} // namespace random_forest 
} // namespace munzekonza 

#endif // MUNZEKONZA_RANDOM_FOREST_TRAINING_RESULT_HPP_
