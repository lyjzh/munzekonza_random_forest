//
// File:          munzekonza/serialization/serialization.hpp
// Author:        Marko Ristin
// Creation date: Sep 15 2014
//

#ifndef MUNZEKONZA_SERIALIZATION_SERIALIZATION_HPP_
#define MUNZEKONZA_SERIALIZATION_SERIALIZATION_HPP_

#include <boost/filesystem/path.hpp>

#include <string>

namespace munzekonza {
namespace serialization {
/// reads a binary archive using boost serialization
template<typename T>
bool read_binary_archive( const std::string& file, T& data_structure );

/// reads a binary archive using boost serialization
template<typename T>
bool read_binary_archive( const boost::filesystem::path& file, T& data_structure );

/// writes to a binary archive using boost serialization
template<typename T>
bool write_binary_archive( const std::string& file, const T& data_structure );

/// writes to a binary archive using boost serialization
template<typename T>
bool write_binary_archive( const boost::filesystem::path& file, const T& data_structure );

} // namespace serialization 
} // namespace munzekonza 

#include "serialization_tpl.hpp"

#endif // MUNZEKONZA_SERIALIZATION_SERIALIZATION_HPP_
