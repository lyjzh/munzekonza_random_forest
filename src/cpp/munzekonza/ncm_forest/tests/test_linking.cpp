//
// File:          cpp/munzekonza/ncm_forest/tests/test_linking.cpp
// Author:        Marko Ristin
// Creation date: May 20 2015
//

#include "munzekonza/mex/mexout.hpp"
#include "munzekonza/mex/array.hpp"
#include "munzekonza/logging/logging.hpp"

#include <mex.h>

/// can throw exceptions
void main_function( int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[] ) {
  munzekonza::Mexout mexout;
  std::cout.rdbuf( &mexout );
  SAY("Hi.");

  SAY("Goodbye.");
}

/// catches all exceptions
void mexFunction( int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[] ) {
  try {
    main_function( nlhs, plhs, nrhs, prhs );
  } catch( std::exception& e ) {
    mexErrMsgTxt( e.what() );
  }
}
