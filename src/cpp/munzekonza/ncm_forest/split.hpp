//
// File:          cpp/class_breakup/ncm_forest/centroids.hpp
// Author:        Marko Ristin
// Creation date: Jul 08 2014
//

#ifndef CPP_CLASS_BREAKUP_NCM_FOREST_SPLIT_HPP_
#define CPP_CLASS_BREAKUP_NCM_FOREST_SPLIT_HPP_

#include "munzekonza/ncm_forest/scalar.hpp"

#include <munzekonza/random_forest/split.hpp>

#include <Eigen/Core>
#include <Eigen/Dense>

#include <boost/serialization/access.hpp>
#include <boost/serialization/base_object.hpp>

namespace munzekonza {

namespace ncm_forest {

/// centroid set of a node
struct Split : public munzekonza::random_forest::Split {
  typedef Eigen::Matrix <
  ScalarD,
  Eigen::Dynamic,
  Eigen::Dynamic,
  Eigen::RowMajor > MatrixD;

  typedef MatrixD SampleD;

  MatrixD centroids;
  std::vector<int> labels;
  std::vector<int> directions;

  int closest_centroid( const MatrixD& sample ) const;
  int direction( const MatrixD& sample ) const;

  virtual Split* create_new() const;

  virtual ~Split() {}

private:
  friend class boost::serialization::access;
  template<class Archive>
  void serialize( Archive& ar, const unsigned int version ) {
    boost::serialization::base_object<munzekonza::random_forest::Split>( *this );
    ar & centroids;
    ar & labels;
    ar & directions;
  }
};

} // namespace ncm_forest
} // namespace munzekonza 

#endif // CPP_CLASS_BREAKUP_NCM_FOREST_SPLIT_HPP_


