//
// File:          cpp/class_breakup/ncm_forest/training.cpp
// Author:        Marko Ristin
// Creation date: Aug 25 2014
//

#include "training.hpp"

namespace munzekonza {

namespace ncm_forest {

void Training::set_rng( boost::mt19937& rng ) {
  rng_ = &rng;
}

void Training::set_data( const MatrixD& descriptors, const std::vector<int>& labels ) {
  descriptors_ = &descriptors;
  labels_ = &labels;
}

} // namespace ncm_forest 

} // namespace munzekonza 

