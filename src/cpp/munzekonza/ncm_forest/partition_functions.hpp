//
// File:          cpp/class_breakup/ncm_forest/partition_functions.hpp
// Author:        Marko Ristin
// Creation date: Aug 11 2014
//

#ifndef CPP_CLASS_BREAKUP_NCM_FOREST_PARTITION_FUNCTIONS_HPP_
#define CPP_CLASS_BREAKUP_NCM_FOREST_PARTITION_FUNCTIONS_HPP_

#include "munzekonza/ncm_forest/scalar.hpp"

#include <Eigen/Core>
#include <Eigen/Dense>

#include <vector>

namespace munzekonza {

namespace ncm_forest {
namespace partition_functions {
typedef Eigen::Matrix < ScalarD,
        Eigen::Dynamic, Eigen::Dynamic,
        Eigen::RowMajor > MatrixD;
} // namespace partition_functions

void compute_centroid(
  const partition_functions::MatrixD& descriptors,
  const std::vector<int>& labels,
  const std::vector<int>& sample_ids,
  int centroid_label,
  Eigen::Ref<partition_functions::MatrixD> centroid );

void compute_centroids(
  const partition_functions::MatrixD& descriptors,
  const std::vector<int>& labels,
  const std::vector<int>& sample_ids,
  const std::vector<int>& centroid_labels,
  partition_functions::MatrixD& centroids );


void compute_partition_class_histos(
  const Eigen::MatrixXi& centroid_class_histos,
  const std::vector<int>& partition,
  Eigen::MatrixXi& partition_class_histos );

/// computes combined entropy of the new and old labels. label_partition indicates for each label_i
/// whether it's a new label (== 1) or an old one (== 0)
double compute_combi_entropy(
  const Eigen::MatrixXi& partition_class_histos,
  double label_weight,
  const std::vector<int>& label_partition );
} // namespace ncm_forest
} // namespace munzekonza 

#endif // CPP_CLASS_BREAKUP_NCM_FOREST_PARTITION_FUNCTIONS_HPP_


