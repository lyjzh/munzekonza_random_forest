//
// File:          cpp/class_breakup/ncm_forest/unittest_partition_iterator.cpp
// Author:        Marko Ristin
// Creation date: Aug 12 2014
//

#include "partition_iterator.hpp"
#include <munzekonza/debugging/assert.hpp>
#include <munzekonza/utils/foreach.hpp>
#define foreach_enumerated MUNZEKONZA_FOREACH_ENUMERATED

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include <vector>

BOOST_AUTO_TEST_CASE( Test_to_binary ) {
  const int ndigits = 5;
  munzekonza::ncm_forest::Partition_iterator_all partition_it(ndigits);

  std::vector<int64_t> golden_binaries;
  for( int i = 0; i < 31; ++i ) {
    golden_binaries.push_back(i);
  }

  std::vector<int64_t> binaries;
  std::vector< std::vector<int> > partitions;
  for(partition_it.start(); !partition_it.done(); partition_it.next()){
    binaries.push_back(partition_it.to_binary());
    partitions.push_back(partition_it.partition());
  }

  foreach_enumerated(int i, int64_t binary, binaries){
    std::vector<int> reconstructed_partition;
    partition_it.partition_from_binary(binary, ndigits, reconstructed_partition);

    for( int j = 0; j < ndigits; ++j ) {
      ASSERT_EQ(reconstructed_partition.at(j), partitions.at(i).at(j));
    }
  }
}


