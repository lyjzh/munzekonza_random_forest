//
// File:          cpp/class_breakup/ncm_forest/training_cvpr.hpp
// Author:        Marko Ristin
// Creation date: Aug 25 2014
//

#ifndef CPP_CLASS_BREAKUP_NCM_FOREST_TRAINING_CVPR_HPP_
#define CPP_CLASS_BREAKUP_NCM_FOREST_TRAINING_CVPR_HPP_

#include "split.hpp"
#include "training.hpp"

#include <boost/random/mersenne_twister.hpp>

namespace munzekonza {

namespace random_forest {
struct Node;
struct Training_result;
} // namespace random_forest

namespace ncm_forest {

class Training_cvpr14 : public Training {
public:
  typedef Split::MatrixD MatrixD;

  Training_cvpr14() :
    min_samples_at_node_( 10 ),
    ncentroids_limit_( -1 ),
    ntests_( 1030 )
  {}

  void set_min_samples_at_node( int min_samples_at_node );
  void set_ncentroids_limit( int ncentroids_limit );
  void set_ntests( int ntests );

  /// trains the node with the given samples
  virtual void train(
    munzekonza::random_forest::Node* node,
    const std::vector<int>& sample_ids,
    munzekonza::random_forest::Split& split,
    munzekonza::random_forest::Training_result& result );

  virtual ~Training_cvpr14() {}

private:
  int min_samples_at_node_;
  int ncentroids_limit_;
  int ntests_;
};
} // namespace ncm_forest
} // namespace munzekonza 

#endif // CPP_CLASS_BREAKUP_NCM_FOREST_TRAINING_CVPR_HPP_


