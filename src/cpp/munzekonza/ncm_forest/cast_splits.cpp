//
// File:          cpp/class_breakup/ncm_forest/cast_splits.cpp
// Author:        Marko Ristin
// Creation date: Aug 04 2014
//

#include "split.hpp"
#include <munzekonza/random_forest/cast_splits.hpp>
#include <munzekonza/random_forest/cast_splits_tpl.hpp>

namespace munzekonza {

namespace random_forest {

template void downcast_splits<ncm_forest::Split>(
  const std::vector<ncm_forest::Split*>& splits,
  std::vector<Split*>& base_splits );

template void upcast_splits<ncm_forest::Split>(
  const std::vector<Split*>& base_splits,
  std::vector<ncm_forest::Split*>& splits );

template void convert_split_map_to_vector<ncm_forest::Split>(
  const std::map<Node*, Split*>& split_map,
  std::vector<ncm_forest::Split*>& splits );

template void convert_splits_to_map<ncm_forest::Split>(
  Node* root, const std::vector<ncm_forest::Split*>& splits,
  std::map<Node*, Split*>& split_map );

} // namespace random_forest


} // namespace munzekonza 
