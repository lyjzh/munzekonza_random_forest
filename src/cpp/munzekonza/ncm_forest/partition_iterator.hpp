//
// File:          cpp/class_breakup/ncm_forest/partition_iterator.hpp
// Author:        Marko Ristin
// Creation date: Jul 08 2014
//

#ifndef CPP_CLASS_BREAKUP_NCM_FOREST_PARTITION_ITERATOR_HPP_
#define CPP_CLASS_BREAKUP_NCM_FOREST_PARTITION_ITERATOR_HPP_

#include <boost/random.hpp>

#include <vector>

namespace munzekonza {

namespace ncm_forest {

class Partition_iterator {
public:
  virtual void start() = 0;
  virtual const std::vector<int>& partition() const = 0;
  virtual void next() = 0;
  virtual bool done() = 0;

  int64_t to_binary() const;

  int i() const;

  /// reconstructs the partition from binary.
  /// does not affect the object itself.
  static void partition_from_binary(
    int64_t binary, int nelements, std::vector<int>& partition );

  virtual ~Partition_iterator() {}

protected:
  int i_;
};

class Partition_iterator_all : public Partition_iterator {
public:
  Partition_iterator_all( int ndigits );

  virtual void start();
  virtual const std::vector<int>& partition() const;
  virtual void next();
  virtual bool done();


private:
  int64_t binary_representation_;
  int64_t max_binary_representation_;

  std::vector<int> partition_;

  void update_partition();
};

class Partition_iterator_random : public Partition_iterator {
public:
  Partition_iterator_random(
    int ndigits, int npartitions, boost::mt19937& rng );
  virtual void start();
  virtual const std::vector<int>& partition() const;
  virtual void next();
  virtual bool done();
private:
  const int npartitions_;
  std::vector<int> partition_;
  int cursor_;
  boost::variate_generator <
    boost::mt19937&, boost::uniform_int<> > rand_assignment_;
};
} // namespace ncm_forest
} // namespace munzekonza 

#endif // CPP_CLASS_BREAKUP_NCM_FOREST_PARTITION_ITERATOR_HPP_


