//
// File:          cpp/class_breakup/ncm_forest/partition_iterator.cpp
// Author:        Marko Ristin
// Creation date: Jul 08 2014
//

#include "partition_iterator.hpp"
#include <munzekonza/debugging/assert.hpp>
#include <munzekonza/logging/logging.hpp>

#include <boost/foreach.hpp>
#define foreach_ BOOST_FOREACH

namespace munzekonza {

namespace ncm_forest {

//
// class Partition_iterator
//
int64_t Partition_iterator::to_binary() const {
  int64_t binary = 0;
  for( int i = 0; i < partition().size(); ++i ) {
    binary = binary | ( ( partition().at( i ) == 1 ) << i );
  }

  return binary;
}

void Partition_iterator::partition_from_binary(
  int64_t binary,
  int nelements,
  std::vector<int>& partition ) {

  ASSERT_LT( nelements, 63 );
  partition.resize( nelements );
  for( int64_t i = 0; i < nelements; ++i ) {
    const int direction = ( binary & ( int64_t(1) << i ) ) >> i;

    if( direction < 0 ) {
      THROW("direction is %d, but must be greater than 0. "
            "binary: %x, i: %d, nelements: %d, mask: %x",
            direction, binary, i, nelements, ( int64_t(1) << i ) );
    }

    ASSERT_GE(direction, 0);
    partition.at( i ) = direction;
  }
}


int Partition_iterator::i() const {
  return i_;
}

//
// class Partition_iterator_all
//
Partition_iterator_all::Partition_iterator_all( int ndigits ) {
  ASSERT_GE( ndigits, 1 );
  ASSERT_LT( ndigits, 63 );
  partition_.resize( ndigits );
  max_binary_representation_ = std::pow( 2, ndigits ) - 1;
}

void Partition_iterator_all::start() {
  binary_representation_ = 0;
  i_ = 0;
  update_partition();
}

const std::vector<int>& Partition_iterator_all::partition() const {
  return partition_;

}

void Partition_iterator_all::next() {
  ASSERT( !done() );

  ++binary_representation_;
  update_partition();

  ++i_;
}

bool Partition_iterator_all::done() {
  return binary_representation_ > max_binary_representation_;

}

void Partition_iterator_all::update_partition() {
  for( int i = 0; i < partition_.size(); ++i ) {
    partition_.at( i ) = ( binary_representation_ & ( 1 << i ) ) >> i;
  }
}

//
// class Partition_iterator_random
//
Partition_iterator_random::Partition_iterator_random( int ndigits, int npartitions, boost::mt19937& rng )  :
  npartitions_( npartitions ),
  cursor_( 0 ),
  rand_assignment_( rng, boost::uniform_int<>( 0, 1 ) ) {

  ASSERT_GE( ndigits, 2 );
  partition_.resize( ndigits );
}

void Partition_iterator_random::start() {
  cursor_ = 0;
  i_ = 0;
}

const std::vector<int>& Partition_iterator_random::partition() const {
  return partition_;
}

void Partition_iterator_random::next() {
  ASSERT( !done() );

  foreach_( int & assignment, partition_ ) {
    assignment = rand_assignment_();
  }
  ++cursor_;
  ++i_;
}

bool Partition_iterator_random::done() {
  return cursor_ >= npartitions_;
}
} // namespace ncm_forest
} // namespace munzekonza 


