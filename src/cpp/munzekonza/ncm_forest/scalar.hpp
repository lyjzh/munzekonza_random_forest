//
// File:          cpp/class_breakup/scalar.hpp
// Author:        Marko Ristin
// Creation date: Jul 09 2014
//

#ifndef CPP_CLASS_BREAKUP_SCALAR_HPP_
#define CPP_CLASS_BREAKUP_SCALAR_HPP_

namespace munzekonza {

namespace ncm_forest {
typedef float ScalarD;
} // namespace ncm_forest 
} // namespace munzekonza 

#endif // CPP_CLASS_BREAKUP_SCALAR_HPP_

