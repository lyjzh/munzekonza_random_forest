//
// File:          cpp/class_breakup/ncm_forest/training.hpp
// Author:        Marko Ristin
// Creation date: Jul 08 2014
//

#ifndef CPP_CLASS_BREAKUP_NCM_FOREST_TRAINING_HPP_
#define CPP_CLASS_BREAKUP_NCM_FOREST_TRAINING_HPP_

#include "split.hpp"

#include <munzekonza/random_forest/training.hpp>

#include <boost/random/mersenne_twister.hpp>

namespace munzekonza {
namespace random_forest {
struct Node;
struct Training_result;
} // namespace random_forest

namespace ncm_forest {

class Training : public munzekonza::random_forest::Training {
public:
  typedef Split::MatrixD MatrixD;

  Training() :
    rng_( NULL ),
    descriptors_( NULL ),
    labels_( NULL )
  {}

  void set_rng( boost::mt19937& rng );

  /// sets the reference to ALL of the data used during the training
  void set_data(
    const MatrixD& descriptors,
    const std::vector<int>& labels );

  /// trains the node with the given samples
  virtual void train(
    munzekonza::random_forest::Node* node,
    const std::vector<int>& sample_ids,
    munzekonza::random_forest::Split& split,
    munzekonza::random_forest::Training_result& result ) = 0;

  virtual ~Training() {}

protected:
  boost::mt19937* rng_;
  const MatrixD* descriptors_;
  const std::vector<int>* labels_;
};
} // namespace ncm_forest
} // namespace munzekonza

#endif // CPP_CLASS_BREAKUP_NCM_FOREST_TRAINING_HPP_


