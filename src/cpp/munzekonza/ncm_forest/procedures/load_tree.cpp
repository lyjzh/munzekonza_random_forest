//
// File:          munzekonza/ncm_forest/procedures/load_tree.cpp
// Author:        Marko Ristin
// Creation date: May 27 2015
//

#include "munzekonza/mex/mexout.hpp"
#include "munzekonza/mex/const_array.hpp"
#include "munzekonza/random_forest/node.hpp"
#include "munzekonza/logging/logging.hpp"
#include "munzekonza/serialization/serialization.hpp"

#include "third_party/matlab_class_wrapper/class_handle.hpp"

#include <mex.h>

namespace rf = munzekonza::random_forest;

/// can throw exceptions
void main_function( int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[] ) {
  munzekonza::Mexout mexout;
  std::cout.rdbuf( &mexout );

  if( nlhs != 1 ) {
    THROW( "Invalid number of left-hand side arguments. "
           "Usage: tree = munzekonza.load_tree(path)" );
  }

  if( nrhs != 1 ) {
    THROW( "Invalid number of right-hand side arguments. "
           "Usage: tree = munzekonza.load_tree(path)" );
  }

  munzekonza::mex::Const_array path_mx( prhs[0] );

  if( !path_mx.is_char() ) THROW( "The argument 'path' must be a char string." );

  rf::Node* root;
  munzekonza::serialization::read_binary_archive( path_mx.to_string(), root );

  plhs[0] = convertPtr2Mat<rf::Node>( root );
}


/// catches all exceptions
void mexFunction( int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[] ) {
  try {
    main_function( nlhs, plhs, nrhs, prhs );
  } catch( std::exception& e ) {
    mexErrMsgTxt( e.what() );
  }
}
