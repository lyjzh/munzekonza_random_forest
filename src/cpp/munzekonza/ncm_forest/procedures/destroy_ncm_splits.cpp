//
// File:          munzekonza/ncm_forest/procedures/destroy_ncm_splits.cpp
// Author:        Marko Ristin
// Creation date: May 20 2015
//

#include "munzekonza/mex/mexout.hpp"
#include "munzekonza/mex/const_array.hpp"
#include "munzekonza/ncm_forest/split.hpp"
#include "munzekonza/logging/logging.hpp"

#include "third_party/matlab_class_wrapper/class_handle.hpp"

#include <mex.h>

/// can throw exceptions
void main_function( int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[] ) {
  munzekonza::Mexout mexout;
  std::cout.rdbuf( &mexout );

  if( nlhs != 0 ) {
    THROW( "Invalid number of left-hand side arguments. "
           "Usage: munzekonza.destroy_ncm_splits(splits)" );
  }

  if( nrhs != 1 ) {
    THROW( "Invalid number of right-hand side arguments. "
           "Usage: munzekonza.destroy_ncm_splits(splits)" );
  }

  typedef munzekonza::ncm_forest::Split SplitD;
  typedef std::vector<SplitD*> Morituri_type;

  munzekonza::mex::Const_array splits_mx(prhs[0]);
  class_handle<Morituri_type>* splits_handle(
    convertMat2HandlePtr<Morituri_type>( splits_mx.get() ));

  if( !splits_mx.is_scalar() || !splits_mx.is_uint64() || splits_mx.is_complex() || !splits_handle->isValid() )
    THROW( "The argument 'splits' must be a valid handle returned by populate_splits_of_ncm_tree(...)." );

  destroyObject<Morituri_type>(splits_mx.get());
}


/// catches all exceptions
void mexFunction( int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[] ) {
  try {
    main_function( nlhs, plhs, nrhs, prhs );
  } catch( std::exception& e ) {
    mexErrMsgTxt( e.what() );
  }
}
