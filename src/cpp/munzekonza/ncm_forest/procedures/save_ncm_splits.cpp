//
// File:          munzekonza/ncm_forest/procedures/save_ncm_splits.cpp
// Author:        Marko Ristin
// Creation date: May 27 2015
//

#include "munzekonza/mex/mexout.hpp"
#include "munzekonza/mex/const_array.hpp"
#include "munzekonza/ncm_forest/split.hpp"
#include "munzekonza/logging/logging.hpp"
#include "munzekonza/serialization/serialization.hpp"
#include "munzekonza/serialization/eigen.hpp"

#include "third_party/matlab_class_wrapper/class_handle.hpp"

#include <mex.h>

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>

typedef munzekonza::ncm_forest::Split SplitD;

/// can throw exceptions
void main_function( int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[] ) {
  munzekonza::Mexout mexout;
  std::cout.rdbuf( &mexout );

  if( nlhs != 0 ) {
    THROW( "Invalid number of left-hand side arguments. "
           "Usage: munzekonza.save_ncm_splits(splits, path)" );
  }

  if( nrhs != 2 ) {
    THROW( "Invalid number of right-hand side arguments. "
           "Usage: munzekonza.save_ncm_splits(splits, path)" );
  }

  munzekonza::mex::Const_array splits_mx( prhs[0] );
  munzekonza::mex::Const_array path_mx( prhs[1] );

  class_handle<std::vector<SplitD*> >* splits_handle(
    convertMat2HandlePtr<std::vector<SplitD*> >( splits_mx.get() ) );

  if( !splits_mx.is_scalar() || !splits_mx.is_uint64() || splits_mx.is_complex() || !splits_handle->isValid() )
    THROW( "The argument 'splits' must be a valid handle returned by train_ncm_tree(...)." );

  if( !path_mx.is_char() ) THROW( "The argument 'path' must be a char string." );

  munzekonza::serialization::write_binary_archive( path_mx.to_string(), splits_handle->ptr() );
}


/// catches all exceptions
void mexFunction( int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[] ) {
  try {
    main_function( nlhs, plhs, nrhs, prhs );
  } catch( std::exception& e ) {
    mexErrMsgTxt( e.what() );
  }
}
