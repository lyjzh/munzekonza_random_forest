//
// File:          munzekonza/numeric/greedy_tsp.cpp
// Author:        Marko Ristin
// Creation date: Sep 11 2014
//

#include "munzekonza/numeric/greedy_tsp.hpp"

#include "munzekonza/utils/foreach.hpp"
#define foreach_in_range MUNZEKONZA_FOREACH_IN_RANGE
#include "munzekonza/debugging/assert.hpp"

#include <boost/numeric/conversion/bounds.hpp>
#include <boost/foreach.hpp>
#define foreach_ BOOST_FOREACH

#include <set>

namespace munzekonza {
namespace numeric {

void Greedy_tsp::compute( Eigen::Ref<Eigen::MatrixXi> distances, std::vector<int>& path ) const {
  ASSERT_EQ( distances.cols(), distances.rows() );

  const int n = distances.cols();

  int current = 0;
  path.reserve( n );
  path.push_back( current );
  std::vector<int> remaining;
  remaining.reserve( n - 1 );
  int nremaining = n - 1;
  foreach_in_range( int i, 1, n ) {
    remaining.push_back( i );
  }

  while( nremaining > 0 ) {
    int min_distance = boost::numeric::bounds<int>::highest();
    int next = -1;
    int next_in_remaining = -1;

    foreach_in_range( int i, 0, nremaining ) {
      const int candidate = remaining.at( i );

      const int distance = distances( current, candidate );
      if( distance < min_distance ) {
        next = candidate;
        min_distance = distance;
        next_in_remaining = i;
      }
    }

    --nremaining;
    std::swap( remaining.at( nremaining ), remaining.at( next_in_remaining ) );

    path.push_back( next );

    current = next;
  }
}

int Greedy_tsp::cost(
  Eigen::Ref<Eigen::MatrixXi> distances,
  const std::vector<int>& path ) const {

  int cost = 0;

  int current = 0;
  for( int next = 1; next < path.size(); ++next ) {
    cost += distances( current, next );
    current = next;
  }

  return cost;
}

} // namespace numeric
} // namespace munzekonza

