#include <string>
#include <string.h>
#include <limits>
#include <list>

#include "munzekonza/parsing/reader.hpp"


using namespace std;

namespace munzekonza {
//
// Reader
//
Reader::Reader() {
  buffer_ = NULL;
}

Reader::Reader(const std::string& str) {
  buffer_ = NULL;
  set(str.c_str());
}

Reader::Reader(const char* str) {
  buffer_ = NULL;
  set(str);
}

void Reader::set( const char* str ) {
  str_ = str;
  has_error_ = false;
  cur_ = 0;
  len_ = strlen( str);

  if( buffer_ != NULL ) {
    delete[] buffer_;
  }
  buffer_ = new char[len_ + 1];
}

int Reader::read_int() {
  if( has_error_ ) return 0;
  if( cur_ >= len_ ) {
    has_error_ = true;
    return 0;
  }
  
  char number[1024];
  int number_i = 0;
  if( str_[cur_] == '-' ) {
    number[0] = '-';
    ++number_i;
    ++cur_;
  }
  
  if( str_[cur_] < '0' || str_[cur_] > '9' ) {
    has_error_ = true;
    return 0;
  }
  
  while( true ) {
    number[number_i] = str_[cur_];
    ++number_i;
    ++cur_;
    
    if( str_[cur_] < '0' || str_[cur_] > '9' ) {
      break;
    }
  }
  
  number[number_i] = 0;
  return std::atoi(number);
}

double Reader::read_numeric() {
  if( has_error() ) {
    return numeric_limits<double>::quiet_NaN();
  }

  double result = read_int();

  if( has_error() ) {
    return numeric_limits<double>::quiet_NaN();
  }

  if( peek( "." ) ) {
    ++cur_;

    int number_length = 0;
    int cur = cur_;
    while( cur < len_ ) {
      if( str_[cur] < '0' || str_[cur] > '9' ) {
        break;
      } else {
        ++number_length;
      }
      ++cur;
    }

    if( number_length == 0 ) {
      has_error_ = true;
      return numeric_limits<double>::quiet_NaN();
    }

    double exponent = 1.0 / 10.0;
    for( int i = 0; i < number_length; ++i ) {
      int digit = 0;
      switch( str_[cur_ + i ] ) {
        case '0': digit = 0; break;
        case '1': digit = 1; break;
        case '2': digit = 2; break;
        case '3': digit = 3; break;
        case '4': digit = 4; break;
        case '5': digit = 5; break;
        case '6': digit = 6; break;
        case '7': digit = 7; break;
        case '8': digit = 8; break;
        case '9': digit = 9; break;
      }

      result += digit * exponent;
      exponent /= 10.0;
    }
    cur_ += number_length;
  }

  return result;
}

void Reader::read_ints(vector<int>& result,
                        const string& separator) {
  std::list<int> ints;

  nibble(" ");
  ints.push_back(read_int());

  if(has_error_) {
    return;
  }

  nibble(" ");
  while(try_read_literal(separator.c_str()) && !has_error_) {
    nibble(" ");
    ints.push_back(read_int());
    nibble(" ");
  }

  if(!has_error_) {
    result.reserve(result.size() + ints.size());
    result.insert(result.end(), ints.begin(), ints.end());
  }

}

void Reader::read_numerics(vector<double>& result, 
                              const string& separator) {
  list<double> nums;

  nums.push_back(read_numeric());

  if(has_error_) {
    return;
  }

  nibble(" ");
  while(try_read_literal(separator.c_str()) && !has_error_) {
    nibble(" ");
    nums.push_back(read_numeric());
    nibble(" ");
  }

  if(!has_error_) {
    result.reserve(result.size() + nums.size());
    result.insert(result.end(), nums.begin(), nums.end());
  }
}

void Reader::read_literal( const char* literal ) {
  if( cur_ >= len_ || has_error_ ) return;
  
  const int literal_len = strlen( literal );
  if(cur_ + literal_len - 1 >= len_) {
    has_error_ = true;
    return;
  }
  
  for(int literal_i = 0; literal_i < literal_len; 
                                      ++literal_i) {      
    if( str_[cur_] != literal[literal_i] ) {
      has_error_ = true;
      return;
    }
    ++cur_;
  }
}

string Reader::read_until( char excluded ) {
  if( has_error_ || cur_ >= len_ ) {
    return "";
  }

  int buffer_cur = 0;

  while( true ) {
    if( cur_ >= len_ ) {
      break;
    }

    if( str_[cur_] == excluded ) {
      break;
    }

    buffer_[ buffer_cur ] = str_[cur_];
    ++cur_;
    ++buffer_cur;
  }
  buffer_[buffer_cur] = 0;

  string result( buffer_ );
  return result;
}

string Reader::read_until( const char* excluded ) {
  if( has_error_ || cur_ >= len_ ) {
    return "";
  }

  const int excluded_length = strlen( excluded );

  int buffer_cur = 0;

  while( cur_ < len_ ) {
    bool should_stop = false;
    for( int i = 0; i < excluded_length; ++i ) {
      if( str_[cur_] == excluded[i] ) {
        should_stop = true;
        break;
      }
    }
    if( should_stop ) {
      break;
    }

    buffer_[ buffer_cur ] = str_[cur_];
    ++cur_;
    ++buffer_cur;
  }
  buffer_[buffer_cur] = 0;

  string result( buffer_ );
  return result;
}
string Reader::read_only( const char* included ) {
  if( has_error_ || cur_ >= len_ ) {
    return "";
  }

  const int included_len = strlen( included );
  if( included_len == 0 ) {
    return "";
  }

  int buffer_cur = 0;

  while( cur_ < len_ ) {
    bool should_stop = false;
    for( int i = 0; i < included_len; ++i ) {
      if( str_[cur_] != included[i] ) {
        should_stop = true;
        break;
      }
    }
    if( should_stop ) {
      break;
    }
    
    buffer_[ buffer_cur ] = str_[cur_];
    ++cur_;
    ++buffer_cur;
  }
  buffer_[buffer_cur] = 0;

  string result( buffer_ );
  return result;
}

string Reader::read_variable() {
  if( has_error_ || cur_ >= len_ ) {
    return "";
  }

  int buffer_cur = 0;

  while( cur_ < len_ ) {
    if( str_[cur_] == '_' ||
        ( str_[cur_] >= 'a' && str_[cur_] <= 'z' ) ||
        ( str_[cur_] >= 'A' && str_[cur_] <= 'Z' ) ||
        ( str_[cur_] >= '0' && str_[cur_] <= '9' ) ) {
      buffer_[ buffer_cur ] = str_[cur_];
      ++cur_;
      ++buffer_cur;
    } else {
      break;
    }
  }

  if( buffer_cur == 0 ) {
    has_error_ = true;
  }

  buffer_[buffer_cur] = 0;

  string result( buffer_ );
  return result;
}

void Reader::nibble( const char* unimportant ) {
  const int unimportant_length = strlen( unimportant );

  bool should_stop = false;
  while( cur_ < len_ && !should_stop ) {
    should_stop = true;
    for( int i = 0; i < unimportant_length; ++i ) {
      if( str_[cur_] == unimportant[i] ) {
        should_stop = false;
        break;
      }
    }
  
    if( !should_stop ) {
      ++cur_;
    }
  }
}

void Reader::nibble_till( const char* important ) {
  const int important_length = strlen( important );

  bool should_stop = false;
  while( cur_ < len_ && !should_stop ) {
    for( int i = 0; i < important_length; ++i ) {
      if( str_[cur_] == important[i] ) {
        should_stop = true;
        break;
      }
    }

    if( !should_stop ) {
      ++cur_;
    }
  }
}

bool Reader::peek( const char* lookahead ) const {
  const int nlookahead = strlen( lookahead );

  if( cur_ + nlookahead > len_ ) {
    return false;
  }

  for( int i = 0; i < nlookahead; ++i ) {
    if( str_[cur_ + i] != lookahead[i] ) {
      return false;
    }
  }

  return true;
}

bool Reader::try_read_literal(const char* literal) {
  bool result = peek(literal);
  if(result) {
    cur_ += strlen(literal);
  }

  return result;
}


bool Reader::has_error() const {
  return has_error_;
}

char Reader::current() const {
  return str_[cur_];
}

const char* Reader::rest() const {
  if( cur_ >= len_ ) return "";

  return &(str_[cur_]);
}

int Reader::cursor() const {
  return cur_;
}

bool Reader::has_finished() const {
  return cur_ == len_;
}

Reader::~Reader() {
  if( buffer_ != NULL ) {
    delete[] buffer_;
  }
}

}

