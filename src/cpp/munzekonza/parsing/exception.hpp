//
// File:          munzekonza/parsing/exception.hpp
// Author:        Marko Ristin
// Creation date: Jul 16 2014
//

#ifndef MUNZEKONZA_PARSING_EXCEPTION_HPP_
#define MUNZEKONZA_PARSING_EXCEPTION_HPP_

#include <exception>
#include <string>

namespace munzekonza {
namespace parsing {

class Exception : public std::exception {
  public:
    Exception(const std::string& what_msg) :
      what_msg_(what_msg) {}

    virtual const char* what() const throw();

    virtual ~Exception() throw() {}

  private:
    const std::string what_msg_;
};

} // namespace parsing 
} // namespace munzekonza 

#endif // MUNZEKONZA_PARSING_EXCEPTION_HPP_