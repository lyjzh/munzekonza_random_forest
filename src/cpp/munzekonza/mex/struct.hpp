//
// File:          munzekonza/mex/struct.hpp
// Author:        Marko Ristin
// Creation date: May 22 2015
//

#ifndef MUNZEKONZA_MEX_STRUCT_HPP_
#define MUNZEKONZA_MEX_STRUCT_HPP_

#include "munzekonza/mex/array.hpp"

#include <vector>
#include <string>
#include <map>

// see extern/include/matrix.h from Matlab directory
typedef struct mxArray_tag mxArray;

namespace munzekonza {
namespace mex {
class Array;

// Wrapper around Matlab structs
class Struct {
public:
  Struct( const mxArray* mx_array );
  Array get_field( const std::string& field ) const;
  std::vector<std::string> field_names() const;

  int number_of_fields() const;
  int number_of_elements() const;

private:
  const mxArray* mx_array_;
  std::map<std::string, int> field_name_to_field_number_;
};
} // namespace mex

} // namespace munzekonza

#endif // MUNZEKONZA_MEX_STRUCT_HPP_
