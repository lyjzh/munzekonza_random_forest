//
// File:          munzekonza/mex/const_array.cpp
// Author:        Marko Ristin
// Creation date: May 23 2015
//

#include "munzekonza/mex/const_array.hpp"

#include "munzekonza/debugging/assert.hpp"
#include "munzekonza/logging/logging.hpp"

#include <mex.h>
#include <matrix.h>

namespace munzekonza {
namespace mex {

Const_array::Const_array( const mxArray* mx_array ) :
  Base_array( mx_array ) {}

const int* Const_array::int_data() const {
  return int_data_impl();
}

const float* Const_array::single_data() const {
  return single_data_impl();
}

const double* Const_array::double_data() const {
  return double_data_impl();
}

const mxArray* Const_array::get() const {
  return const_mx_array_;
}

Eigen::Map <const Eigen::MatrixXf> Const_array::to_single_matrix_map() const {
  return Eigen::Map <const Eigen::MatrixXf>( single_data(), nrows(), ncols() );
}

Eigen::Map <const Eigen::MatrixXd> Const_array::to_double_matrix_map() const {
  return Eigen::Map <const Eigen::MatrixXd>( double_data(), nrows(), ncols() );
}

} // namespace mex
} // namespace munzekonza
