//
// File:          munzekonza/mex/array.cpp
// Author:        Marko Ristin
// Creation date: May 06 2015
//

#include "munzekonza/mex/array.hpp"

#include "munzekonza/debugging/assert.hpp"
#include "munzekonza/logging/logging.hpp"

#include <mex.h>
#include <matrix.h>

namespace munzekonza {
namespace mex {

Array::Array( mxArray* mx_array ) :
  Base_array( mx_array ) {}

int* Array::int_data() {
  return const_cast<int*>( int_data_impl() );
}

float* Array::single_data() {
  return const_cast<float*>( single_data_impl() );
}

double* Array::double_data() {
  return const_cast<double*>( double_data_impl() );
}

mxArray* Array::get() {
  return const_cast<mxArray*>( const_mx_array_ );
}

Eigen::Map <Eigen::MatrixXf> Array::to_single_matrix_map() {
  return Eigen::Map <Eigen::MatrixXf>( single_data(), nrows(), ncols() );
}

Eigen::Map <Eigen::MatrixXd> Array::to_double_matrix_map() {
  return Eigen::Map <Eigen::MatrixXd>( double_data(), nrows(), ncols() );
}

} // namespace mex
} // namespace munzekonza
