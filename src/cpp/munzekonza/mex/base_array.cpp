//
// File:          munzekonza/mex/base_array.cpp
// Author:        Marko Ristin
// Creation date: May 23 2015
//

#include "munzekonza/mex/base_array.hpp"
#include "munzekonza/mex/struct.hpp"
#include "munzekonza/debugging/assert.hpp"
#include "munzekonza/logging/logging.hpp"

#include <mex.h>
#include <matrix.h>

namespace munzekonza {
namespace mex {
Base_array::Base_array( const mxArray* mx_array ) :
  const_mx_array_( mx_array )
{}

bool Base_array::is_matrix() const {
  const mwSize ndims = mxGetNumberOfDimensions( const_mx_array_ );
  return ndims == 2 && mxIsNumeric( const_mx_array_ );
}

bool Base_array::is_int() const {
  return mxIsInt32( const_mx_array_ );
}

bool Base_array::is_uint64() const {
  return mxGetClassID( const_mx_array_ ) == mxUINT64_CLASS;
}

bool Base_array::is_single() const {
  return mxIsSingle( const_mx_array_ );
}

bool Base_array::is_double() const {
  return mxIsDouble( const_mx_array_ );
}

bool Base_array::is_complex() const {
  return mxIsComplex( const_mx_array_ );
}

bool Base_array::is_char() const {
  return mxIsChar( const_mx_array_ );
}

bool Base_array::is_struct() const {
  return mxIsStruct( const_mx_array_ );
}

bool Base_array::is_scalar() const {
  const mwSize ndims = mxGetNumberOfDimensions( const_mx_array_ );
  const mwSize* size = mxGetDimensions( const_mx_array_ );

  bool result = true;
  for( int dim_i = 0; dim_i < ndims; ++dim_i ) {
    result = result && size[dim_i] == 1;
  }

  return result && mxIsNumeric( const_mx_array_ );
}

bool Base_array::is_vector() const {
  if( !is_matrix() ) {
    return false;
  }

  const mwSize* size = mxGetDimensions( const_mx_array_ );
  const int nrows = size[0];
  const int ncols = size[1];

  return ( ( nrows == 1 && ncols >= 1 ) || ( nrows >= 1 && ncols == 1 ) ) &&
         mxIsNumeric( const_mx_array_ );
}

int Base_array::nrows() const {
  const mwSize* size = mxGetDimensions( const_mx_array_ );
  return size[0];
}

int Base_array::ncols() const {
  const mwSize* size = mxGetDimensions( const_mx_array_ );
  return size[1];
}

int Base_array::length() const {
  return mxGetNumberOfElements( const_mx_array_ );
}

double Base_array::to_scalar() const {
  return mxGetScalar( const_mx_array_ );
}

template<typename T, typename U>
void data_to_vector( T* data, int n, std::vector<U>& vec ) {

  vec.reserve( vec.size() + n );
  for( int i = 0; i < n; ++i ) {
    vec.push_back( static_cast<U>( data[i] ) );
  }
}

template<typename T>
void Base_array::to_vector_impl( std::vector<T>& vec ) const {
  ASSERT( is_vector() );

  const int n = length();

  if( is_int() ) {
    data_to_vector( int_data_impl(), n, vec );
  } else if( is_single() ) {
    data_to_vector( single_data_impl(), n, vec );
  } else if( is_double() ) {
    data_to_vector( double_data_impl(), n, vec );
  } else {
    THROW( "No conversion implemented from %s in to_vector.", mxGetClassName( const_mx_array_ ) );
  }
}

void Base_array::to_vector( std::vector<int>& vec ) const {
  to_vector_impl( vec );
}

void Base_array::to_vector( std::vector<float>& vec ) const {
  to_vector_impl( vec );
}

void Base_array::to_vector( std::vector<double>& vec ) const {
  to_vector_impl( vec );
}

std::string Base_array::to_string() const {
  ASSERT( is_char() );

  const int length =  mxGetNumberOfElements( const_mx_array_ );

  std::string result;
  result.resize( length );
  mxGetString( const_mx_array_, &result[0], length + 1 );

  return result;
}

Struct Base_array::to_struct() const {
  ASSERT( is_struct() );
  return Struct( const_mx_array_ );
}

const int* Base_array::int_data_impl() const {
  ASSERT( is_int() );
  return static_cast<const int*>( mxGetData( const_mx_array_ ) );
}

const float* Base_array::single_data_impl() const {
  ASSERT( is_single() );
  return static_cast<const float*>( mxGetData( const_mx_array_ ) );
}

const double* Base_array::double_data_impl() const {
  ASSERT( is_double() );
  return static_cast<const double*>( mxGetData( const_mx_array_ ) );
}

} // namespace mex 
} // namespace munzekonza 
