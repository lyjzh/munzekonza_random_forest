//
// File:          munzekonza/mex/mex.hpp
// Author:        Marko Ristin
// Creation date: May 06 2015
//

#ifndef MUNZEKONZA_MEX_MEX_HPP_
#define MUNZEKONZA_MEX_MEX_HPP_

#include <mex.h>

namespace munzekonza {
namespace mex {
/// \returns true if the array is a 2D matrix with the given number of rows and columns.
bool isMatrixOfSize(mxArray* array, int nrows, int ncols) {
  const mwSize ndims = mxGetNumberOfDimensions(array);
  if(ndims != 2) {
    return false;
  }

  const mwSize* size = mxGetDimensions(array);
  if(size[0] != nrows || size[1] != ncols) {
    return false;
  }

  return true;
}
} // namespace mex 
} // namespace munzekonza 

#endif // MUNZEKONZA_MEX_MEX_HPP_
