//
// File:          munzekonza/mex/array.hpp
// Author:        Marko Ristin
// Creation date: May 06 2015
//

#ifndef MUNZEKONZA_MEX_ARRAY_HPP_
#define MUNZEKONZA_MEX_ARRAY_HPP_

#include "munzekonza/mex/base_array.hpp"

namespace munzekonza {
namespace mex {

/// wrapper around mxArray
class Array : public Base_array {
public:
  Array( mxArray* mx_array );

  int* int_data();
  float* single_data();
  double* double_data();

  mxArray* get();

  Eigen::Map <Eigen::MatrixXf> to_single_matrix_map();
  Eigen::Map <Eigen::MatrixXd> to_double_matrix_map();
};
} // namespace mex
} // namespace munzekonza

#endif // MUNZEKONZA_MEX_ARRAY_HPP_
