//
// File:          munzekonza/mex/struct.cpp
// Author:        Marko Ristin
// Creation date: May 22 2015
//

#include "munzekonza/mex/struct.hpp"
#include "munzekonza/mex/array.hpp"
#include "munzekonza/debugging/assert.hpp"
#include "munzekonza/logging/logging.hpp"
#include <munzekonza/utils/foreach.hpp>
#define foreach_in_map MUNZEKONZA_FOREACH_IN_MAP
#define foreach_value MUNZEKONZA_FOREACH_VALUE
#define foreach_key MUNZEKONZA_FOREACH_KEY
#define foreach_enumerated MUNZEKONZA_FOREACH_ENUMERATED
#define foreach_in_range MUNZEKONZA_FOREACH_IN_RANGE

#include <mex.h>
#include <matrix.h>

namespace munzekonza {
namespace mex {

Struct::Struct( const mxArray* mx_array ) :
  mx_array_( mx_array ) {

  const int nfields = number_of_fields();
  if(number_of_elements() != 1) {
    THROW("You are trying to access an array struct with more than one element which is not supported yet.");
  }

  for( int field_i = 0; field_i < nfields; ++field_i ) {
    const std::string field_name = mxGetFieldNameByNumber( mx_array_, field_i );
    field_name_to_field_number_[field_name] = field_i;
  }
}

Array Struct::get_field( const std::string& field ) const {
  if(field_name_to_field_number_.count(field) == 0) {
    THROW("You are trying to access the field '%s' which is not present in the struct.", field);
  }

  const int field_i = field_name_to_field_number_.at(field);
  mxArray* field_mx = mxGetFieldByNumber( mx_array_, 0, field_i);

  return Array(field_mx);
}

std::vector<std::string> Struct::field_names() const {
  std::vector<std::string> result;
  result.reserve(number_of_fields());
  foreach_key(const std::string& field_name, field_name_to_field_number_) {
    result.push_back(field_name);
  }

  return result;
}

int Struct::number_of_fields() const {
  return mxGetNumberOfFields( mx_array_ );
}

int Struct::number_of_elements() const {
  return mxGetNumberOfElements( mx_array_ );
}

} // namespace mex
} // namespace munzekonza
