//
// File:          munzekonza/utils/unittest_tictoc.cpp
// Author:        Marko Ristin
// Creation date: Sep 11 2014
//

#include "munzekonza/utils/tictoc.hpp"

#include "munzekonza/debugging/assert.hpp"

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include <vector>
#include <chrono>
#include <thread>

BOOST_AUTO_TEST_CASE( Test_toc ) {
  munzekonza::Tictoc t_start;

  std::this_thread::sleep_for( std::chrono::seconds( 2 ) );

  const double time = t_start.toc();

  ASSERT_EQ_ABSPRECISION( time, 2.0, 0.001 );
}

