//
// File:          munzekonza/utils/unittest_foreach_in_list.cpp
// Author:        Marko Ristin
// Creation date: Feb 07 2013
//

#include "munzekonza/utils/foreach.hpp"
#define foreach_enumerated MUNZEKONZA_FOREACH_ENUMERATED

#include "munzekonza/logging/logging.hpp"
#include "munzekonza/debugging/assert.hpp"

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include <boost/assign/list_of.hpp>
#include <boost/foreach.hpp>
#define foreach_ BOOST_FOREACH

#include <list>

BOOST_AUTO_TEST_CASE( Test_foreach_in_list ) {
  std::list<int> golden_values = boost::assign::list_of(2)(4)(6);

  std::vector<int> indices;
  std::vector<int> values;

  foreach_enumerated(const int index, int value, golden_values) {
    indices.push_back(index);
    values.push_back(value);
  }

  ASSERT_EQ( indices.size(), 3 );
  ASSERT_EQ( indices[0], 0 );
  ASSERT_EQ( indices[1], 1 );
  ASSERT_EQ( indices[2], 2 );
  
  ASSERT_EQ( values.size(), 3 );
  ASSERT_EQ( values[0], 2 );
  ASSERT_EQ( values[1], 4 );
  ASSERT_EQ( values[2], 6 );

  // test reference
  indices.clear();
  values.clear();

  foreach_enumerated(const int index, int& value, golden_values) {
    value = (index + 1) * 10;
  }

  foreach_enumerated(const int index, int value, golden_values) {
    indices.push_back(index);
    values.push_back(value);
  }

  ASSERT_EQ( indices.size(), 3 );
  ASSERT_EQ( indices[0], 0 );
  ASSERT_EQ( indices[1], 1 );
  ASSERT_EQ( indices[2], 2 );
  
  ASSERT_EQ( values.size(), 3 );
  ASSERT_EQ( values[0], 10 );
  ASSERT_EQ( values[1], 20 );
  ASSERT_EQ( values[2], 30 );
}

BOOST_AUTO_TEST_CASE( Test_break ) {
  std::vector<int> values = boost::assign::list_of
                            (1) (2) (3) (4) (5) (6) (7) (8);



  int count = 0;


  //size_t _index = 0;
  //const auto _end = (values).end();
  //auto _it = (values).begin();
  //bool _broke = false;

  //for(bool _continue = true; 
  //    _continue && 
  //        _it != _end && 
  //        !_broke;
  //    (_continue) ? munzekonza::foreach::increase_index(_it, 
  //                                                      _index) : (void)0)
  //  if( munzekonza::foreach::set_false(_continue)) {} else
  //    if(bool _switch = false) {} else
  //    for(int value = *_it; 
  //        !_continue; 
  //        _continue = true)
  //      for(int myindex = _index; 
  //          !_switch; 
  //          _broke = false) 
  //      if(munzekonza::foreach::set_true(_switch)) {} else
  //      if(munzekonza::foreach::set_true(_broke)) {} else {
  //      DUMP( myindex, value );
  //      if( value % 2 == 0 && value > 0) {
  //        ++count;
  //        DUMP( count );
  //        break;
  //      }
  //    }

  foreach_enumerated(int index, int value, values) {
    ASSERT_EQ( index, value - 1 );
    if( index % 2 == 0 && index > 0) {
      ++count;
      break;
    }
  }

  ASSERT_EQ( count, 1 );
}

