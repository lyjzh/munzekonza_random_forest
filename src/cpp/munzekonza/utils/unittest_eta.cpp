//
// File:          munzekonza/utils/unittest_eta.cpp
// Author:        Marko Ristin
// Creation date: Aug 29 2014
//

#include "munzekonza/utils/eta.hpp"

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include <chrono>
#include <thread>

BOOST_AUTO_TEST_CASE( Test_percentage ) {

  const int n = 800;

  munzekonza::Eta eta( n );
  eta.add_handler_on_percentage( 3, new munzekonza::eta::Report( __FILE__, __LINE__, "oioi" ) );

  for( int i = 0; i < n; ++i ) {
    eta.heart_beat();
    std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );
  }
}

