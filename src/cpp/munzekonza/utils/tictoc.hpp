#ifndef MUNZEKONZA__UTILS__TICTOC__HTICTOC__HPP_
#define MUNZEKONZA__UTILS__TICTOC__HTICTOC__HPP_

#include <chrono>

namespace munzekonza {

class Tictoc {
public:
  Tictoc();

  void tic();
  double toc();
  double toc( const char* msg );
  double last_lap() const;

private:
  typedef std::chrono::duration<double, std::ratio<1> > LapT;
  std::chrono::high_resolution_clock::time_point t_;
  LapT last_lap_;
};


}

#endif
