//
// File:          munzekonza/utils/unittest_foreach_in_range.cpp
// Author:        Marko Ristin
// Creation date: Aug 14 2014
//

#include "munzekonza/utils/foreach.hpp"
#define foreach_in_range MUNZEKONZA_FOREACH_IN_RANGE

#include "munzekonza/debugging/assert.hpp"

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include <vector>

BOOST_AUTO_TEST_CASE( Test_foreach_in_range ) {
  std::vector<int> golden(10,0);
  for( int i = 2; i < golden.size(); ++i ) {
    golden.at(i) = i;
  }

  std::vector<int> mine(10, 0);
  foreach_in_range(int i, 2, 10) {
    mine.at(i) = i;
  }

  ASSERT_EQ(mine.size(), golden.size());
  for( int i = 0; i < 10; ++i ) {
    ASSERT_EQ(golden.at(i), mine.at(i));
  }
}

