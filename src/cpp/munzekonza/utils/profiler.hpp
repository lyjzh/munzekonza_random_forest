//
// File:          munzekonza/utils/profiler.hpp
// Author:        Marko Ristin
// Creation date: Sep 11 2014
//

#ifndef MUNZEKONZA_UTILS_PROFILER_HPP_
#define MUNZEKONZA_UTILS_PROFILER_HPP_

#include "tictoc.hpp"

#include <map>
#include <string>

namespace munzekonza {

class Profiler{
public:
  Profiler();

  void entry();
  void exit();

  void start(const std::string& name);
  void stop(const std::string& name);

  void reset();

  std::string analyze() const;

  double time(const std::string& name) const;

  ~Profiler();

private:
  double total_time_;
  munzekonza::Tictoc t_entry_;

  std::map<std::string, Tictoc*> t_;
  std::map<std::string, double> times_;

  Tictoc& get_t(const std::string& name);

  std::string time_str(double time) const;
};

} // namespace munzekonza 

#endif // MUNZEKONZA_UTILS_PROFILER_HPP_
