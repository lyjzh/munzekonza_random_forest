#include "munzekonza/utils/foreach.hpp"
#define foreach_in_map MUNZEKONZA_FOREACH_IN_MAP
#define foreach_key MUNZEKONZA_FOREACH_KEY
#define foreach_value MUNZEKONZA_FOREACH_VALUE

#include "munzekonza/debugging/assert.hpp"
#include "munzekonza/logging/logging.hpp"
#include "munzekonza/utils/tictoc.hpp"

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include <boost/typeof/typeof.hpp>
#include <boost/lexical_cast.hpp>

#include <vector>
#include <map>


using namespace std;
using munzekonza::Tictoc;

BOOST_AUTO_TEST_CASE( Test_MUNZEKONZA_FOREACH_IN_MAP1 ) {
  std::map<int, std::string> mymap;

  mymap[0] = "oi";
  mymap[1] = "noi";

  std::map<int, std::string> newmap;

  foreach_in_map(int key, std::string& value, mymap) {
    newmap[key] = value;
  }

  ASSERT_EQ( newmap.size(), 2 );
  ASSERT_EQ( newmap.count(0), 1 );
  ASSERT_EQ( newmap.count(1), 1 );
  ASSERT_EQ( newmap.at(0), "oi" );
  ASSERT_EQ( newmap.at(1), "noi" );
}

BOOST_AUTO_TEST_CASE( Test_MUNZEKONZA_FOREACH_IN_MAP2 ) {
  std::map<int, std::string> mymap;
  std::map<int, std::string> newmap;

  foreach_in_map(int key, std::string& value, mymap) {
    newmap[key] = value;
  }

  ASSERT_EQ( newmap.size(), 0 );
}

BOOST_AUTO_TEST_CASE( Test_MUNZEKONZA_FOREACH_IN_MAP3 ) {
  std::map<int, std::string> mymap;
  for( int i = 0; i < 10 * 1000 * 1000; ++i ) {
    mymap[i] = "oi";
  }

  int index0 = 0;
  Tictoc t_original;
  for(auto it = mymap.begin(); it != mymap.end(); ++it) {
    ASSERT_EQ( it->first, index0 );
    ASSERT_EQ( it->second, "oi" );
    ++index0;
  }
  t_original.toc( "original" );

  int index1 = 0;
  Tictoc t_new_implementation;
  foreach_in_map(int key, std::string& value, mymap) {
    ASSERT_EQ( key, index1 );
    ASSERT_EQ( value, "oi" );
    ++index1;
  }
  t_new_implementation.toc( "new_implementation" );
}

BOOST_AUTO_TEST_CASE( Test_MUNZEKONZA_FOREACH_IN_MAP4 ) {
  std::map<int, std::string> mymap;

  mymap[0] = "oi";
  mymap[1] = "noi";

  std::map<int, std::string> newmap;

  foreach_in_map(int key, std::string& value, mymap) {
    value = "voronoi" + boost::lexical_cast<std::string>(key);
  }

  ASSERT_EQ( mymap.size(), 2 );
  ASSERT_EQ( mymap.count(0), 1 );
  ASSERT_EQ( mymap.count(1), 1 );
  ASSERT_EQ( mymap.at(0), "voronoi0" );
  ASSERT_EQ( mymap.at(1), "voronoi1" );
}

BOOST_AUTO_TEST_CASE( Test_MUNZEKONZA_FOREACH_KEY1 ) {
  std::map<int, std::string> mymap;

  mymap[0] = "oi";
  mymap[1] = "noi";

  std::vector<int> keys;

  foreach_key(int key, mymap) {
    keys.push_back(key);
  }

  ASSERT_EQ( keys.size(), 2 );
  ASSERT_EQ( keys[0], 0 );
  ASSERT_EQ( keys[1], 1 );
}

BOOST_AUTO_TEST_CASE( Test_MUNZEKONZA_FOREACH_VALUE1 ) {
  std::map<int, std::string> mymap;

  mymap[0] = "oi";
  mymap[1] = "noi";

  std::vector<std::string> values;

  foreach_value(const std::string& value, mymap) {
    values.push_back(value);
  }

  ASSERT_EQ( values.size(), 2 );
  ASSERT_EQ( values[0], "oi" );
  ASSERT_EQ( values[1], "noi" );
}

BOOST_AUTO_TEST_CASE( Test_MUNZEKONZA_FOREACH_VALUE2 ) {
  std::map<int, std::string> mymap;

  mymap[0] = "oi";
  mymap[1] = "noi";

  std::vector<std::string> values;

  int i = 0;
  foreach_value(std::string& value, mymap) {
    value = "voronoi" + boost::lexical_cast<std::string>(i);
    ++i;
  }

  ASSERT_EQ( mymap.size(), 2 );
  ASSERT_EQ( mymap.count(0), 1 );
  ASSERT_EQ( mymap.count(1), 1 );
  ASSERT_EQ( mymap.at(0), "voronoi0" );
  ASSERT_EQ( mymap.at(1), "voronoi1" );
}

BOOST_AUTO_TEST_CASE( Test_foreach_in_map_break ) {
  std::map<int, int> mymap;
  mymap[0] = 1;
  mymap[1] = 2;
  mymap[2] = 3;
  mymap[3] = 4;
  mymap[4] = 5;
  mymap[5] = 6;
  mymap[6] = 7;
  mymap[7] = 8;




  int count = 0;

  //const auto _end = (mymap).end();
  //auto _it = (mymap).begin();
  //bool _broke = false;

  //for(bool _continue = true; 
  //    _continue && 
  //        _it != _end && 
  //        !_broke;
  //    (_continue) ? munzekonza_foreach::increase_it(_it) : (void)0)
  //  if( munzekonza_foreach::set_false(_continue)) {} else
  //  if( bool _switch = false) {} else
  //  for(int key = _it->first; 
  //        !_continue; 
  //        _continue = true)
  //    for(int value = _it->second;
  //        !_switch;
  //        _broke = false)
  //      if(munzekonza_foreach::set_true(_switch)) {} else 
  //      if(munzekonza_foreach::set_true(_broke)) {} else {




  //        DUMP( key, value );
  //        if( key % 2 == 0 && key > 0 ) {
  //          DUMP( count );
  //          ++count;
  //          break;
  //        }
  //    }

  foreach_in_map(int key, int value, mymap) {
    if( key % 2 == 0 && key > 0 ) {
      ASSERT_EQ( key, value - 1 );
      ++count;
      break;
    }
  }

  ASSERT_EQ( count, 1 );
}

BOOST_AUTO_TEST_CASE( Test_foreach_key_break ) {
  std::map<int, int> mymap;
  mymap[0] = 1;
  mymap[1] = 2;
  mymap[2] = 3;
  mymap[3] = 4;
  mymap[4] = 5;
  mymap[5] = 6;
  mymap[6] = 7;
  mymap[7] = 8;

  int count = 0;

  foreach_key(int key, mymap) {
    if( key % 2 == 0 && key > 0 ) {
      ++count;
      break;
    }
  }

  ASSERT_EQ( count, 1 );
}
BOOST_AUTO_TEST_CASE( Test_foreach_value_break ) {
  std::map<int, int> mymap;
  mymap[0] = 1;
  mymap[1] = 2;
  mymap[2] = 3;
  mymap[3] = 4;
  mymap[4] = 5;
  mymap[5] = 6;
  mymap[6] = 7;
  mymap[7] = 8;

  int count = 0;

  //const auto _end = (mymap).end();
  //auto _it = (mymap).begin();

  //for(bool _continue = true; 
  //    _continue && 
  //        _it != _end;
  //    (_continue) ? munzekonza_foreach::increase_it(_it) : (void)0)
  //  if( munzekonza_foreach::set_false(_continue)) {} else
  //  for(int value = _it->first; 
  //        !_continue; 
  //        _continue = true) {
  //        DUMP( value );
  //        if( value % 2 == 0 && value > 0 ) {
  //          DUMP( count );
  //          ++count;
  //          break;
  //        }
  //    }

  foreach_value(int value, mymap) {
    if( value % 2 == 0 && value > 0 ) {
      ++count;
      break;
    }
  }

  ASSERT_EQ( count, 1 );
}

