//
// File:          munzekonza/utils/eta.cpp
// Author:        Marko Ristin
// Creation date: Aug 29 2014
//

#include "munzekonza/utils/eta.hpp"

#include <boost/foreach.hpp>
#define foreach_ BOOST_FOREACH
#include <boost/format.hpp>

#include <chrono>
#include <ctime>
#include <sstream>
#include <ostream>
#include <iomanip>


namespace munzekonza {

namespace eta {
void Report::set_out( std::ostream& out ) {
  out_ = &out;
}

void Report::fire( const Eta& eta ) {
  ( *out_ ) << source_code_path_ << ":" << line_number_ << ": ";

  if( !caption_.empty() ) {
    ( *out_ ) << caption_ << ": ";
  }

  std::stringstream etr_ss;
  if( eta.etr() <= 60.0 ) {
    etr_ss << boost::format( "%.1f" ) % eta.etr() << " seconds";
  } else if( eta.etr() <= 3600.0 ) {
    etr_ss << boost::format( "%.1f" ) % ( eta.etr() / 60.0 ) << " minutes";
  } else {
    etr_ss << boost::format( "%.1f" ) % ( eta.etr() / 3600.0 ) << " hours";
  }
  const std::string etr_str = etr_ss.str();

  std::stringstream n_ss;
  n_ss << eta.n();
  const int n_length = n_ss.str().size();
  const std::string counter_format = ( boost::format( "%%%dd" ) % n_length ).str();

  ( *out_ ) << boost::format( counter_format ) % eta.counter() << " / " << eta.n() <<
            " (" << boost::format( "%5.1f" ) % eta.percentage_finished() << "% / 100.0%)" <<
            ", ETA: " << eta.eta() <<
            ", ETR: " << etr_str <<
            std::endl;

}
} // namespace eta


Eta::Eta( int n, int chunk_size ) :
  n_( n ),
  chunk_size_( chunk_size ),
  counter_( 0 ),
  start_( std::chrono::system_clock::now() )
{}

Eta::Eta( int n ) :
  n_( n ),
  chunk_size_( ( n < 100 ) ? 1 : int( float( n ) / 100.f + 0.5f ) ),
  counter_( 0 ),
  start_( std::chrono::system_clock::now() )
{}

void Eta::start() {
  counter_ = 0;
  start_ = std::chrono::system_clock::now();
}

void Eta::heart_beat() {
  std::lock_guard<std::mutex> acquire_heart_beat_lock(
    heart_beat_lock_ );

  ++counter_;
  if( counter_ % chunk_size_ != 0 && counter_ != n_ ) {
    return;
  }

  update_watch();

  foreach_( eta::Wrapper & wrapper, wrappers_ ) {
    if( counter_ >= wrapper.next_firing ) {
      wrapper.handler->fire( *this );
      wrapper.next_firing = std::min( n_, wrapper.next_firing + wrapper.chunk_size );
    }
  }
}

void Eta::update_watch() {
  std::lock_guard<std::mutex> acquire_update_watch_lock(
    update_watch_lock_ );

  const std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
  seconds_passed_ = std::chrono::duration_cast<std::chrono::seconds>( now - start_ );

  const double items_remaining = n_ - counter_;
  const double avg_time_per_item = double( seconds_passed_.count() ) / double( counter_ );

  seconds_remaining_ = std::chrono::seconds( int( avg_time_per_item * items_remaining + 0.5 ) );
  eta_ = now + seconds_remaining_;
}

void Eta::add_handler_on_percentage( int percentage_freq, eta::Event_handler* event_handler ) {
  const int handler_chunk_size = int( double( n_ ) / 100.0 * double( percentage_freq ) + 0.5 );
  add_handler_every( handler_chunk_size, event_handler );
}

void Eta::add_handler_every( int handler_chunk_size, eta::Event_handler* event_handler ) {
  eta::Wrapper wrapper;
  wrapper.chunk_size = handler_chunk_size;
  wrapper.next_firing = std::min( n_, counter_ + handler_chunk_size );
  wrapper.handler = event_handler;

  wrappers_.push_back( wrapper );
}

std::string Eta::eta( std::string format ) const {
  auto in_time_t = std::chrono::system_clock::to_time_t( eta_ );

  struct tm* timeinfo;

  const int max_buffer_length = 512;
  char buffer [max_buffer_length];

  timeinfo = localtime ( &in_time_t );
  strftime( buffer, max_buffer_length, format.c_str(), timeinfo );
  return std::string( buffer );
}

double Eta::etr() const {
  return seconds_remaining_.count();
}

int Eta::counter() const {
  return counter_;
}

int Eta::n() const {
  return n_;
}

int Eta::chunk_size() const {
  return chunk_size_;
}

float Eta::percentage_finished() const {
  return 100.f * float( counter_ ) / float( n_ );
}

Eta::~Eta() {
  foreach_( eta::Wrapper & wrapper, wrappers_ ) {
    if( wrapper.handler != NULL ) {
      delete wrapper.handler;
    }
  }
}

} // namespace munzekonza

